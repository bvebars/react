import React from 'react'
import Footer from './Footer'
import AddTodo from '../containers/AddTodo'
import VisibleTodoList from '../containers/VisibleTodoList'
import Body from './Body'

const App = () => (
  <div>
  	<Body />
    <AddTodo />
    <VisibleTodoList />
    <Footer />
  </div>
)

export default App